# FontAwesome

FontAwesome for QML

To use FontAwesome in QML, please clone this repository to your project.
```bash
git clone https://gitcode.com/PassXYZ/FontAwesome.git
```

Add font in your CMakefile as below:
```
qt_add_resources(${CMAKE_PROJECT_NAME} "configuration"
    PREFIX "/"
    FILES
        FontAwesome/font//fontawesome-webfont.ttf
        qtquickcontrols2.conf)

```

In your QML code, you can use FontAwesome as below:
```qml
import QtQuick
import QtQuick.Controls
import QEdit
import FontAwesome

ApplicationWindow {
    width: mainScreen.width
    height: mainScreen.height

    visible: true
    title: "QEdit"

    FontAwesome {
        id: awesome
        resource: "qrc:///FontAwesome/font/fontawesome-webfont.ttf"
        // resource: "http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/fonts/fontawesome-webfont.ttf"
    }

...

    footer: Text
    {
        x: 24; y: 24
        text: awesome.icons.fa_amazon
        font.family: awesome.family
        font.pixelSize: 48
        horizontalAlignment: Text.AlignHCenter
    }

```